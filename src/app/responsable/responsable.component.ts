import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import { HeaderService } from '../servicios/header.servicio';
import {SelectionModel} from '@angular/cdk/collections';

export interface Food {
  value: string;
  viewValue: string;
}

export interface ResultadosActivo { 
  nombre_responsable: string;
  cargo_responsable: string;
  correo_responsable: string;
  fingreso_responsable: string;
  ciudad_responsable: string;
  sede_responsable: string;
  activos_responsable: number;
  valorneto_responsable: number;



}

const ELEMENT_DATA: ResultadosActivo[] = [
  {nombre_responsable: 'Victor San Juan', cargo_responsable:'Admin', correo_responsable: 'soporte@smartassets.com.co', fingreso_responsable: '2016-01-01', ciudad_responsable:'Bogotá', sede_responsable:'Bogotá', activos_responsable:11, valorneto_responsable:0,},
  {nombre_responsable: 'Victor San Juan', cargo_responsable:'Admin', correo_responsable: 'soporte@smartassets.com.co', fingreso_responsable: '2016-01-01', ciudad_responsable:'Bogotá', sede_responsable:'Bogotá', activos_responsable:11, valorneto_responsable:0,},
  {nombre_responsable: 'Victor San Juan', cargo_responsable:'Admin', correo_responsable: 'soporte@smartassets.com.co', fingreso_responsable: '2016-01-01', ciudad_responsable:'Bogotá', sede_responsable:'Bogotá', activos_responsable:11, valorneto_responsable:0,},
  {nombre_responsable: 'Victor San Juan', cargo_responsable:'Admin', correo_responsable: 'soporte@smartassets.com.co', fingreso_responsable: '2016-01-01', ciudad_responsable:'Bogotá', sede_responsable:'Bogotá', activos_responsable:11, valorneto_responsable:0,},
  {nombre_responsable: 'Victor San Juan', cargo_responsable:'Admin', correo_responsable: 'soporte@smartassets.com.co', fingreso_responsable: '2016-01-01', ciudad_responsable:'Bogotá', sede_responsable:'Bogotá', activos_responsable:11, valorneto_responsable:0,},
  {nombre_responsable: 'Victor San Juan', cargo_responsable:'Admin', correo_responsable: 'soporte@smartassets.com.co', fingreso_responsable: '2016-01-01', ciudad_responsable:'Bogotá', sede_responsable:'Bogotá', activos_responsable:11, valorneto_responsable:0,},
  {nombre_responsable: 'Victor San Juan', cargo_responsable:'Admin', correo_responsable: 'soporte@smartassets.com.co', fingreso_responsable: '2016-01-01', ciudad_responsable:'Bogotá', sede_responsable:'Bogotá', activos_responsable:11, valorneto_responsable:0,},
  {nombre_responsable: 'Victor San Juan', cargo_responsable:'Admin', correo_responsable: 'soporte@smartassets.com.co', fingreso_responsable: '2016-01-01', ciudad_responsable:'Bogotá', sede_responsable:'Bogotá', activos_responsable:11, valorneto_responsable:0,},
  {nombre_responsable: 'Victor San Juan', cargo_responsable:'Admin', correo_responsable: 'soporte@smartassets.com.co', fingreso_responsable: '2016-01-01', ciudad_responsable:'Bogotá', sede_responsable:'Bogotá', activos_responsable:11, valorneto_responsable:0,},
  {nombre_responsable: 'Victor San Juan', cargo_responsable:'Admin', correo_responsable: 'soporte@smartassets.com.co', fingreso_responsable: '2016-01-01', ciudad_responsable:'Bogotá', sede_responsable:'Bogotá', activos_responsable:11, valorneto_responsable:0,},
  {nombre_responsable: 'Victor San Juan', cargo_responsable:'Admin', correo_responsable: 'soporte@smartassets.com.co', fingreso_responsable: '2016-01-01', ciudad_responsable:'Bogotá', sede_responsable:'Bogotá', activos_responsable:11, valorneto_responsable:0,},

  ];


@Component({
  selector: 'app-responsable',
  templateUrl: './responsable.component.html',
  styleUrls: ['./responsable.component.css']
})
export class ResponsableComponent implements OnInit {

@ViewChild(MatPaginator) paginator: MatPaginator;
@ViewChild(MatSort) sort: MatSort;

  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }


  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }	

  displayedColumns: string[] = ['select', 'nombre_responsable', 'cargo_responsable', 'correo_responsable', 'fingreso_responsable', 'ciudad_responsable', 'sede_responsable','activos_responsable', 'valorneto_responsable'];
  dataSource = new MatTableDataSource<ResultadosActivo>(ELEMENT_DATA);
  selection = new SelectionModel<ResultadosActivo>(true, []);
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }




  constructor(private headerService: HeaderService, public dialog: MatDialog) {}

   openDialog() {
    const dialogRef = this.dialog.open(hojadevidadialog, {panelClass: 'hoja-de-vida'});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }



  ngOnInit() {
  	  this.headerService.setTitle('Responsable');
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}

//Dialog Hoja de vida
@Component({
  selector: 'hojadevidadialog',
  templateUrl: 'hojadevida.dialog.html',
})
export class hojadevidadialog {}
