import { PaginasWebComponent } from '../paginas-web/paginas-web.component';
import { FormulariosComponent } from '../formularios/formularios.component';
import { DashboardComponent } from '../dashboard/dashboard.component';

import { BuscarActivoComponent } from '../buscar-activo/buscar-activo.component';
import { CrearActivoComponent } from '../crear-activo/crear-activo.component';
import { ResponsableComponent } from '../responsable/responsable.component';
import { AlarmasComponent } from '../alarmas/alarmas.component';
import { UbicacionCiudadComponent } from '../ubicacion-ciudad/ubicacion-ciudad.component';
import { UbicacionSedeComponent } from '../ubicacion-sede/ubicacion-sede.component';
import { AprobacionesComponent } from '../aprobaciones/aprobaciones.component';
import { InformesComponent } from '../informes/informes.component';
import { AuditoriaComponent } from '../auditoria/auditoria.component';
import { ConciliacionComponent } from '../conciliacion/conciliacion.component';
import { ConfiguracionComponent } from '../configuracion/configuracion.component';
import { HojaActivoComponent } from '../hoja-activo/hoja-activo.component';
import { EditarActivoComponent  } from '../editar-activo/editar-activo.component';
import { HojaResponsableComponent  } from '../hoja-responsable/hoja-responsable.component';
import { CrearResponsableComponent  } from '../crear-responsable/crear-responsable.component';
import { MantenimientoComponent  } from '../mantenimiento/mantenimiento.component';
import { MantenimientoOpcionesComponent } from '../mantenimiento-opciones/mantenimiento-opciones.component';
import { CrearMantenimientoComponent } from '../crear-mantenimiento/crear-mantenimiento.component';

import { ModuleWithProviders } from '@angular/core';
import { Route , RouterModule } from '@angular/router' ;
const rutas: Route[] = [


    {
        path : 'dashboard',
        component : DashboardComponent,
        data: { title: 'Inicio' }
    },
    { 
        path: '', 
        redirectTo: 'dashboard', 
        pathMatch: 'full' 
    },
    
    {
        path : 'crear-activo',
        component : CrearActivoComponent
    },

    {
        path : 'buscar-activo',
        component : BuscarActivoComponent
    },

    {
        path : 'responsable',
        component : ResponsableComponent
    },


    {
        path : 'alarmas',
        component : AlarmasComponent
    },


    {
        path : 'ubicacion-ciudad',
        component : UbicacionCiudadComponent
    },

    {
        path : 'ubicacion-sede',
        component : UbicacionSedeComponent
    },

    {
        path : 'aprobaciones',
        component : AprobacionesComponent
    },
    {
        path : 'informes',
        component :  InformesComponent 
    },
    {
        path : 'auditoria',
        component :  AuditoriaComponent 
    },
    {
        path : 'conciliacion',
        component : ConciliacionComponent 
    },
    {
        path : 'configuracion',
        component : ConfiguracionComponent 
    },

    {
        path : 'activo/hoja-de-vida',
        component : HojaActivoComponent 
    },
    {
        path : 'activo/editar-activo',
        component : EditarActivoComponent 
    },
    {
        path : 'responsable/hoja-responsable',
        component :  HojaResponsableComponent
    },

    {
        path : 'mantenimiento/opciones',
        component : MantenimientoOpcionesComponent
    },

    {
        path : 'mantenimiento',
        component : MantenimientoComponent
    },
    {
        path : 'mantenimiento/crear',
        component : CrearMantenimientoComponent
    },

    {
        path : 'responsable/crear',
        component : CrearResponsableComponent
    },


 

   
];
export const Routing: ModuleWithProviders = RouterModule.forRoot(rutas);