import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../servicios/header.servicio';

@Component({
  selector: 'app-conciliacion',
  templateUrl: './conciliacion.component.html',
  styleUrls: ['./conciliacion.component.css']
})
export class ConciliacionComponent implements OnInit {

  constructor(private headerService: HeaderService  ) { }

  ngOnInit() {
    this.headerService.setTitle('Conciliación');
  }

}
