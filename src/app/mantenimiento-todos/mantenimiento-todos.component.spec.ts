import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MantenimientoTodosComponent } from './mantenimiento-todos.component';

describe('MantenimientoTodosComponent', () => {
  let component: MantenimientoTodosComponent;
  let fixture: ComponentFixture<MantenimientoTodosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MantenimientoTodosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MantenimientoTodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
