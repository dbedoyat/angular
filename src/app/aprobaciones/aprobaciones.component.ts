import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import { HeaderService } from '../servicios/header.servicio';
import {SelectionModel} from '@angular/cdk/collections';

export interface Food {
  value: string;
  viewValue: string;
}

export interface NuevosActivo {
  codigo_activo: string;
  nombre_activo: string;
  neto_activo: number;
  sede_activo: string;
  realizado_activo:string;
  fecha_activo: string;
  estatus_activo: string;
  aprobar_activo: string;
  eliminar_activo: string;

}

const ELEMENT_DATA: NuevosActivo[] = [
  {codigo_activo: 'DM-001934', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', neto_activo:0.00, sede_activo:'Bodega Principal', realizado_activo: 'Leidy Johana Camargo', fecha_activo: '2018-10-03', estatus_activo:'Pendiente', aprobar_activo:'check', eliminar_activo: 'delete_forever'},
  {codigo_activo: 'DM-001933', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', neto_activo:0.00, sede_activo:'Bodega Principal', realizado_activo: 'Leidy Johana Camargo', fecha_activo: '2018-10-03', estatus_activo:'Pendiente', aprobar_activo:'check', eliminar_activo: 'delete_forever'},
  {codigo_activo: 'DM-001932', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', neto_activo:0.00, sede_activo:'Bodega Principal', realizado_activo: 'Leidy Johana Camargo', fecha_activo: '2018-10-03', estatus_activo:'Pendiente', aprobar_activo:'check', eliminar_activo: 'delete_forever'},
  {codigo_activo: 'DM-001931', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', neto_activo:0.00, sede_activo:'Bodega Principal', realizado_activo: 'Leidy Johana Camargo', fecha_activo: '2018-10-03', estatus_activo:'Pendiente', aprobar_activo:'check', eliminar_activo: 'delete_forever'},
  {codigo_activo: 'DM-001924', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', neto_activo:0.00, sede_activo:'Bodega Principal', realizado_activo: 'Leidy Johana Camargo', fecha_activo: '2018-10-03', estatus_activo:'Pendiente', aprobar_activo:'check', eliminar_activo: 'delete_forever'},
  {codigo_activo: 'DM-001914', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', neto_activo:0.00, sede_activo:'Bodega Principal', realizado_activo: 'Leidy Johana Camargo', fecha_activo: '2018-10-03', estatus_activo:'Pendiente', aprobar_activo:'check', eliminar_activo: 'delete_forever'},
  {codigo_activo: 'DM-001944', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', neto_activo:0.00, sede_activo:'Bodega Principal', realizado_activo: 'Leidy Johana Camargo', fecha_activo: '2018-10-03', estatus_activo:'Pendiente', aprobar_activo:'check', eliminar_activo: 'delete_forever'},
  {codigo_activo: 'DM-001954', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', neto_activo:0.00, sede_activo:'Bodega Principal', realizado_activo: 'Leidy Johana Camargo', fecha_activo: '2018-10-03', estatus_activo:'Pendiente', aprobar_activo:'check', eliminar_activo: 'delete_forever'},
  {codigo_activo: 'DM-001964', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', neto_activo:0.00, sede_activo:'Bodega Principal', realizado_activo: 'Leidy Johana Camargo', fecha_activo: '2018-10-03', estatus_activo:'Pendiente', aprobar_activo:'check', eliminar_activo: 'delete_forever'},
  {codigo_activo: 'DM-001974', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', neto_activo:0.00, sede_activo:'Bodega Principal', realizado_activo: 'Leidy Johana Camargo', fecha_activo: '2018-10-03', estatus_activo:'Pendiente', aprobar_activo:'check', eliminar_activo: 'delete_forever'},

  ];

@Component({
  selector: 'app-aprobaciones',
  templateUrl: './aprobaciones.component.html',
  styleUrls: ['./aprobaciones.component.css']
})
export class AprobacionesComponent implements OnInit {
	@ViewChild(MatPaginator) paginator: MatPaginator;
  	@ViewChild(MatSort) sort: MatSort;


  	isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];


  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }	

  displayedColumns: string[] = ['select', 'codigo_activo', 'nombre_activo', 'neto_activo', 'sede_activo', 'realizado_activo', 'fecha_activo', 'estatus_activo', 'aprobar_activo', 'eliminar_activo',  ];
  dataSource = new MatTableDataSource<NuevosActivo>(ELEMENT_DATA);
  selection = new SelectionModel<NuevosActivo>(true, []);
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }



  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Aprobaciones');
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

 

}
