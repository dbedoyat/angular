import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UbicacionCiudadComponent } from './ubicacion-ciudad.component';

describe('UbicacionCiudadComponent', () => {
  let component: UbicacionCiudadComponent;
  let fixture: ComponentFixture<UbicacionCiudadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UbicacionCiudadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UbicacionCiudadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
