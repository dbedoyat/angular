import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../servicios/header.servicio';



@Component({
  selector: 'app-paginas-web',
  templateUrl: './paginas-web.component.html',
  styleUrls: ['./paginas-web.component.css']
})
export class PaginasWebComponent implements OnInit {


  constructor(private headerService: HeaderService) { }



	ngOnInit() {
		  this.headerService.setTitle('Paginas Web');
	}



}
