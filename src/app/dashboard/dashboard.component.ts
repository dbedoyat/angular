import {Component, OnInit, ViewChild} from '@angular/core';
import { HeaderService } from '../servicios/header.servicio';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';


export interface Food {
  value: string;
  viewValue: string;
}

export interface NotificacionesActivo { 
  codigo_activo: string;
  nombre_activo: string;
  tipo_activo: string;
  sede_activo: string;
  valor_activo: number;
  iva_activo: number;
  responsable_activo: string;

}



const ELEMENT_DATA:NotificacionesActivo[] = [
  {codigo_activo: 'DM-001934', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001933', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001932', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001931', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001924', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001914', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001944', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001954', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001964', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001974', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},

  ];


  
@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
  })


  

export class DashboardComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];


  title: string = 'dashboard-map';
  lat: number = 4.1895251;
  lng: number = -74.4178952;



  //datos del diagrama torta1 - https://valor-software.com/ng2-charts/
  public pieChartLabels:string[] = ['Pitalito', 'Cali', 'Bogotá', 'Floresta', 'Sede2', 'Única'];
  public pieChartData:number[] = [24.4, 20.5, 9.4, 6.6, 10.1, 40.0];
  public pieChartType:string = 'pie';

 //datos del diagrama torta2 - https://valor-software.com/ng2-charts/
  public doughnutChartLabels:string[] =['Pitalito', 'Cali', 'Bogotá', 'Floresta', 'Sede2', 'Única'];
  public doughnutChartData:number[] = [24.4, 20.5, 9.4, 6.6, 10.1, 40.0];
  public doughnutChartType:string = 'doughnut';

   // Radar
   public radarChartLabels:string[] = ['Bueno', 'Regular', 'Malo'];
 
   public radarChartData:any = [
     {data: [65, 70 , 47,], label: 'Distribución por sedes'},
   ];

   public chartColors: Array<any> = [
    { // all colors in order
      backgroundColor: ['#4FC3F7', '#03A9F4', '#64B5F6', '#2196F3', '#7986CB', '#3F51B5']
    }
]
  
public chartline: Array<any> = [
  { // all colors in order
    borderColor	: ['#4FC3F7'],
    backgroundColor: ['rgba(79, 195, 247, 0.2)']
  }
]
 

   public radarChartType:string = 'radar';

//Bar dashboard
   public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels:string[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo',];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
  
 
  public barChartData:any[] = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Actualizaciones por día'}
  ];
 


  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }

  
/* Tabla */ 
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }


  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }	

  displayedColumns: string[] = ['select', 'codigo_activo', 'nombre_activo', 'tipo_activo', 'sede_activo', 'valor_activo', 'iva_activo', 'responsable_activo',  ];
  dataSource = new MatTableDataSource<NotificacionesActivo>(ELEMENT_DATA);
  selection = new SelectionModel<NotificacionesActivo>(true, []);
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }





  constructor(private headerService: HeaderService) { 

  }

  ngOnInit() {

    this.headerService.setTitle('Dashboard');
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
