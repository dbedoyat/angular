import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MantenimientoVencidosComponent } from './mantenimiento-vencidos.component';

describe('MantenimientoVencidosComponent', () => {
  let component: MantenimientoVencidosComponent;
  let fixture: ComponentFixture<MantenimientoVencidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MantenimientoVencidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MantenimientoVencidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
