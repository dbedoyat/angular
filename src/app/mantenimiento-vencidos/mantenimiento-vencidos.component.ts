import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';


export interface  mantenimientoVencidos {
  activo_mantenimiento: string;
  fecha_mantenimiento: string;
  editar_mantenimiento:  string;  
  activar_mantenimiento:string ;


}

const ELEMENT_DATA:  mantenimientoVencidos[] = [

{ activo_mantenimiento: 'Activo001',
fecha_mantenimiento: '2018-08-01',
editar_mantenimiento: 'string',
activar_mantenimiento:'string',

},

{ activo_mantenimiento: 'Activo001',
fecha_mantenimiento: '2018-08-01',
editar_mantenimiento: 'string',
activar_mantenimiento:'string',

},

{ activo_mantenimiento: 'Activo001',
fecha_mantenimiento: '2018-08-01',
editar_mantenimiento: 'string',
activar_mantenimiento:'string',

},
];



@Component({
  selector: 'app-mantenimiento-vencidos',
  templateUrl: './mantenimiento-vencidos.component.html',
  styleUrls: ['./mantenimiento-vencidos.component.css']
})
export class MantenimientoVencidosComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] = [ 'activo_mantenimiento', 'fecha_mantenimiento', 'editar_mantenimiento', 'activar_mantenimiento', ];
  dataSource = new MatTableDataSource< mantenimientoVencidos>(ELEMENT_DATA);
  selection = new SelectionModel< mantenimientoVencidos>(true, []);
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  constructor() { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
