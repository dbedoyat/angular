import { NgModule, ApplicationRef } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Routing } from './servicios/rutas';
import { CommonModule } from '@angular/common';
import { BrowserModule }    from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { HeaderService } from './servicios/header.servicio';
import { ChartsModule } from 'ng2-charts';


import { AppComponent } from './app.component';

import { PaginasWebComponent } from './paginas-web/paginas-web.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BarraSuperiorComponent } from './barra-superior/barra-superior.component';
import { FormulariosComponent } from './formularios/formularios.component';
import { BuscarActivoComponent, BajaActivo } from './buscar-activo/buscar-activo.component';
import { CrearActivoComponent } from './crear-activo/crear-activo.component';
import { ResponsableComponent, hojadevidadialog } from './responsable/responsable.component';
import { AlarmasComponent } from './alarmas/alarmas.component';
import { UbicacionCiudadComponent } from './ubicacion-ciudad/ubicacion-ciudad.component';
import { UbicacionSedeComponent } from './ubicacion-sede/ubicacion-sede.component';
import { AprobacionesComponent } from './aprobaciones/aprobaciones.component';
import { AuditoriaComponent} from './auditoria/auditoria.component';

import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';


import { AgmCoreModule } from '@agm/core';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 1,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },

};

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

//Material Angular
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { BuscarActivoRapidoComponent } from './buscar-activo-rapido/buscar-activo-rapido.component';
import { ConciliacionComponent } from './conciliacion/conciliacion.component';
import { InformesComponent } from './informes/informes.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { MantenimientoComponent } from './mantenimiento/mantenimiento.component';
import { HojaActivoComponent, BajaActivoHv } from './hoja-activo/hoja-activo.component';
import { EditarActivoComponent } from './editar-activo/editar-activo.component';
import { HojaResponsableComponent } from './hoja-responsable/hoja-responsable.component';
import { GrillaAlarmasComponent } from './grilla-alarmas/grilla-alarmas.component';
import { GrillaActivosComponent } from './grilla-activos/grilla-activos.component';
import { GrillaResponsableComponent } from './grilla-responsable/grilla-responsable.component';
import { GrillaResultadoActivoComponent } from './grilla-resultado-activo/grilla-resultado-activo.component';
import { MantenimientoTodosComponent } from './mantenimiento-todos/mantenimiento-todos.component';
import { MantenimientoVencidosComponent } from './mantenimiento-vencidos/mantenimiento-vencidos.component';
import { MantenimientoPendientesComponent } from './mantenimiento-pendientes/mantenimiento-pendientes.component';
import { MantenimientoRealizadosComponent } from './mantenimiento-realizados/mantenimiento-realizados.component';
import { MantenimientoOpcionesComponent, BajaMantenimiento } from './mantenimiento-opciones/mantenimiento-opciones.component';
import { CrearResponsableComponent } from './crear-responsable/crear-responsable.component';
import { CrearMantenimientoComponent } from './crear-mantenimiento/crear-mantenimiento.component';



@NgModule({
  exports: [
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule
  ],
  declarations: [], //Si crean un componente mediante consola (ng generate component "nombre"), recomiendo mover los elementos generados aquí, en el siguiente "Declarations"

})
export class MaterialModule {}

@NgModule({
  declarations: [
    AppComponent,
    PaginasWebComponent,
    DashboardComponent,
    BarraSuperiorComponent,
    FormulariosComponent, 
    BuscarActivoComponent,
    CrearActivoComponent, 
    ResponsableComponent,
    AlarmasComponent, 
    UbicacionCiudadComponent,
    UbicacionSedeComponent, 
    AprobacionesComponent, 
    AuditoriaComponent,
    BuscarActivoRapidoComponent,
    hojadevidadialog,
    BajaActivo,
    BajaActivoHv,
    BajaMantenimiento,
    ConciliacionComponent, 
    InformesComponent,
    ConfiguracionComponent, 
    MantenimientoComponent,
    HojaActivoComponent,
    EditarActivoComponent,
    HojaResponsableComponent,
    GrillaAlarmasComponent, 
    GrillaActivosComponent, 
    GrillaResponsableComponent, 
    GrillaResultadoActivoComponent,
    MantenimientoTodosComponent, 
    MantenimientoVencidosComponent, 
    MantenimientoPendientesComponent, 
    MantenimientoRealizadosComponent,
    MantenimientoOpcionesComponent,
    CrearResponsableComponent,
    CrearMantenimientoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    Routing,
    BrowserAnimationsModule,
    MaterialModule,
    ChartsModule,
    SwiperModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAILzd7CeETLtcmedEC3ApV9E0HWOA_qVk'
    })

    

  ],

  entryComponents: [
    hojadevidadialog,
    BajaActivo,
    BajaActivoHv,
    BajaMantenimiento
  ],
  providers: [HeaderService,

    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
