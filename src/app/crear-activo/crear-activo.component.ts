import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import { HeaderService } from '../servicios/header.servicio';
import {SelectionModel} from '@angular/cdk/collections';

export interface Food {
  value: string;
  viewValue: string;
}


export interface ResultadosActivo {
  codigo_activo: string;
  nombre_activo: string;
  tipo_activo: string;
  sede_activo: string;
  valor_activo: number;
  iva_activo: number;
  responsable_activo: string;



}

const ELEMENT_DATA: ResultadosActivo[] = [
  {codigo_activo: 'DM-001934', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001933', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001932', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001931', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001924', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001914', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001944', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001954', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001964', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},
  {codigo_activo: 'DM-001974', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},

  ];



@Component({
  selector: 'app-crear-activo',
  templateUrl: './crear-activo.component.html',
  styleUrls: ['./crear-activo.component.css']
})
export class CrearActivoComponent implements OnInit {

  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

	
btnSave(event){
    event.target.classList.add('active');
    setTimeout(() => {

      event.target.classList.remove('active');
  }, 2900);  

}

isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataSource.data.length;
  return numSelected === numRows;
}

masterToggle() {
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
}	

displayedColumns: string[] = ['select', 'codigo_activo', 'nombre_activo', 'tipo_activo', 'sede_activo', 'valor_activo', 'iva_activo', 'responsable_activo',  ];
dataSource = new MatTableDataSource<ResultadosActivo>(ELEMENT_DATA);
selection = new SelectionModel<ResultadosActivo>(true, []);
applyFilter(filterValue: string) {
  this.dataSource.filter = filterValue.trim().toLowerCase();
}


	foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];	


  constructor(private headerService: HeaderService) { }

  


  ngOnInit() {
    this.headerService.setTitle('Crear Activo');  
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

}
