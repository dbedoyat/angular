import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import { HeaderService } from '../servicios/header.servicio';
import {SelectionModel} from '@angular/cdk/collections';

export interface ResultadosActivo {
  codigo_activo: string;
  nombre_activo: string;
  tipo_activo: string;
  sede_activo: string;
  valor_activo: number;
  iva_activo: number;
  responsable_activo: string;



}

const ELEMENT_DATA: ResultadosActivo[] = [
  {codigo_activo: 'DM-001934', nombre_activo: 'DISPENSADOR DE SOLUCION LAVADO IH', tipo_activo:'Dispositivos Medicos (DM)', sede_activo:'Bodega Principal', valor_activo: 0, iva_activo: 0, responsable_activo:'RUEDA ROJAS JUAN EMILIO'},

  ];




@Component({
  selector: 'app-hoja-responsable',
  templateUrl: './hoja-responsable.component.html',
  styleUrls: ['./hoja-responsable.component.css']
})
export class HojaResponsableComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] = [ 'codigo_activo', 'nombre_activo', 'tipo_activo', 'sede_activo', 'valor_activo', 'iva_activo', 'responsable_activo',  ];
  dataSource = new MatTableDataSource<ResultadosActivo>(ELEMENT_DATA);
  selection = new SelectionModel<ResultadosActivo>(true, []);
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  constructor(private headerService: HeaderService) {}

  ngOnInit() {
    this.headerService.setTitle('HV responsable');
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }


}
