import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HojaResponsableComponent } from './hoja-responsable.component';

describe('HojaResponsableComponent', () => {
  let component: HojaResponsableComponent;
  let fixture: ComponentFixture<HojaResponsableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HojaResponsableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HojaResponsableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
