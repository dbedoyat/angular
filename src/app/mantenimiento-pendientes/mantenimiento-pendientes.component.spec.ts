import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MantenimientoPendientesComponent } from './mantenimiento-pendientes.component';

describe('MantenimientoPendientesComponent', () => {
  let component: MantenimientoPendientesComponent;
  let fixture: ComponentFixture<MantenimientoPendientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MantenimientoPendientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MantenimientoPendientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
