import { Component, OnInit } from '@angular/core';

export interface Food {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-buscar-activo-rapido',
  templateUrl: './buscar-activo-rapido.component.html',
  styleUrls: ['./buscar-activo-rapido.component.css']
})
export class BuscarActivoRapidoComponent implements OnInit {

foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];	


  constructor() { }

  ngOnInit() {
  }

}
