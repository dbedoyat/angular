import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscarActivoRapidoComponent } from './buscar-activo-rapido.component';

describe('BuscarActivoRapidoComponent', () => {
  let component: BuscarActivoRapidoComponent;
  let fixture: ComponentFixture<BuscarActivoRapidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscarActivoRapidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscarActivoRapidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
