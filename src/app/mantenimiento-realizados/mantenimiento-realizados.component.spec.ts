import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MantenimientoRealizadosComponent } from './mantenimiento-realizados.component';

describe('MantenimientoRealizadosComponent', () => {
  let component: MantenimientoRealizadosComponent;
  let fixture: ComponentFixture<MantenimientoRealizadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MantenimientoRealizadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MantenimientoRealizadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
