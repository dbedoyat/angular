import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';

export interface mantenimientoTodos {
  activo_mantenimiento: string;
  fecha_mantenimiento: string;
  editar_mantenimiento:  string;  
  activar_mantenimiento:string ;


}

const ELEMENT_DATA: mantenimientoTodos[] = [

{ activo_mantenimiento: 'Activo001',
fecha_mantenimiento: '2018-08-01',
editar_mantenimiento: 'string',
activar_mantenimiento:'string',

},

{ activo_mantenimiento: 'Activo001',
fecha_mantenimiento: '2018-08-01',
editar_mantenimiento: 'string',
activar_mantenimiento:'string',

},

{ activo_mantenimiento: 'Activo001',
fecha_mantenimiento: '2018-08-01',
editar_mantenimiento: 'string',
activar_mantenimiento:'string',

},
];

@Component({
  selector: 'app-mantenimiento-realizados',
  templateUrl: './mantenimiento-realizados.component.html',
  styleUrls: ['./mantenimiento-realizados.component.css']
})
export class MantenimientoRealizadosComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] = [ 'activo_mantenimiento', 'fecha_mantenimiento', 'editar_mantenimiento', 'activar_mantenimiento', ];
  dataSource = new MatTableDataSource<mantenimientoTodos>(ELEMENT_DATA);
  selection = new SelectionModel<mantenimientoTodos>(true, []);
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  constructor() { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
