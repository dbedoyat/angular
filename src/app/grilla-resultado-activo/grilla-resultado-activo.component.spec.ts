import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrillaResultadoActivoComponent } from './grilla-resultado-activo.component';

describe('GrillaResultadoActivoComponent', () => {
  let component: GrillaResultadoActivoComponent;
  let fixture: ComponentFixture<GrillaResultadoActivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrillaResultadoActivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrillaResultadoActivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
