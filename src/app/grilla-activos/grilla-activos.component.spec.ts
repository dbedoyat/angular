import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrillaActivosComponent } from './grilla-activos.component';

describe('GrillaActivosComponent', () => {
  let component: GrillaActivosComponent;
  let fixture: ComponentFixture<GrillaActivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrillaActivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrillaActivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
