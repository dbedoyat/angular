import { Component, OnInit } from '@angular/core';

export interface Food {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-editar-activo',
  templateUrl: './editar-activo.component.html',
  styleUrls: ['./editar-activo.component.css']
})
export class EditarActivoComponent implements OnInit {
	foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];	

	
  btnSave(event){
    event.target.classList.add('active');
    setTimeout(() => {

      event.target.classList.remove('active');
  }, 2900);  

}



  constructor() { }

  ngOnInit() {
  }

}
