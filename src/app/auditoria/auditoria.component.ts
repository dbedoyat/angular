import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import { HeaderService } from '../servicios/header.servicio';
import {SelectionModel} from '@angular/cdk/collections';
import {FormControl} from '@angular/forms';


export interface Food {
  value: string;
  viewValue: string;
}

export interface AuditoriaGrilla {
  ciudad_auditoria: string;
  sede_auditoria: string;
  zonas_auditoria: number;
  fecha_auditoria: string;
  responsable_auditoria:string ;
  activos_auditoria: number;
  valor_auditoria: number;
  estado_auditoria: string;
  auditores_auditoria: number;
  cargar_auditoria: string;
  editar_auditoria: string;

}

const ELEMENT_DATA: AuditoriaGrilla[] = [

  { ciudad_auditoria: 'string',
    sede_auditoria: 'string',
    zonas_auditoria: 1,
    fecha_auditoria:'string',
    responsable_auditoria:'string',
    activos_auditoria: 0,
    valor_auditoria: 0,
    estado_auditoria: 'string',
    auditores_auditoria: 1,
    cargar_auditoria:'person',
    editar_auditoria:'edit',
  },

];
@Component({
  selector: 'app-auditoria',
  templateUrl: './auditoria.component.html',
  styleUrls: ['./auditoria.component.css']
})
export class AuditoriaComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  auditZona = new FormControl();

  auditZonaList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];

  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  btnSave(event){
    event.target.classList.add('active');
    setTimeout(() => {

      event.target.classList.remove('active');
  }, 2900);  

}
 



  displayedColumns: string[] = [ 'ciudad_auditoria', 'sede_auditoria', 'zonas_auditoria', 'fecha_auditoria', 'responsable_auditoria', 'activos_auditoria', 'valor_auditoria','estado_auditoria',  'auditores_auditoria', 'cargar_auditoria',  'editar_auditoria',];
  dataSource = new MatTableDataSource<AuditoriaGrilla>(ELEMENT_DATA);
  selection = new SelectionModel<AuditoriaGrilla>(true, []);
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private headerService: HeaderService, public dialog: MatDialog) {}

 

  ngOnInit() {
    this.headerService.setTitle('Auditoría');
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

}

