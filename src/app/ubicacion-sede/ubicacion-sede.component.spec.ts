import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UbicacionSedeComponent } from './ubicacion-sede.component';

describe('UbicacionSedeComponent', () => {
  let component: UbicacionSedeComponent;
  let fixture: ComponentFixture<UbicacionSedeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UbicacionSedeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UbicacionSedeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
        