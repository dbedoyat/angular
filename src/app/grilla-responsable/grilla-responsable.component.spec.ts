import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrillaResponsableComponent } from './grilla-responsable.component';

describe('GrillaResponsableComponent', () => {
  let component: GrillaResponsableComponent;
  let fixture: ComponentFixture<GrillaResponsableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrillaResponsableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrillaResponsableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
