import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../servicios/header.servicio';

@Component({
  selector: 'app-formularios',
  templateUrl: './formularios.component.html',
  styleUrls: ['./formularios.component.css']
})
export class FormulariosComponent implements OnInit {


  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Formularios');
  }

}
 