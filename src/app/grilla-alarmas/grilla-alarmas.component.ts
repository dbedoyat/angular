import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';

//Crear interfaz de variables
export interface grillaAlarmas {
  nombre_alarma: string;
  descripcion_alarma: string;
  tipo_alarma: string;
  inicio_alarma: string;
  ejecucion_alarma: string;
  repeticion_alarma: number;
  nejecuciones_alarma: number;
  nactivos_alarma: number;
}

//Información local de alarma
const ELEMENT_DATA: grillaAlarmas [] = [
  { nombre_alarma:'string',
    descripcion_alarma: 'string',
    tipo_alarma: 'string',
    inicio_alarma:'string',
    ejecucion_alarma:'string',
    repeticion_alarma:1,
    nejecuciones_alarma: 2,
    nactivos_alarma: 3,
  }
  
];

@Component({
  selector: 'app-grilla-alarmas',
  templateUrl: './grilla-alarmas.component.html',
  styleUrls: ['./grilla-alarmas.component.css']
})
export class GrillaAlarmasComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  
  displayedColumns: string[] = [ 'nombre_alarma', 'descripcion_alarma', 'tipo_alarma', 'inicio_alarma', 'ejecucion_alarma', 'repeticion_alarma', 'nejecuciones_alarma',  'nactivos_alarma',  ];
  
  dataSource = new MatTableDataSource<grillaAlarmas>(ELEMENT_DATA);
  selection = new SelectionModel<grillaAlarmas>(true, []);
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }



  constructor() { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

}
