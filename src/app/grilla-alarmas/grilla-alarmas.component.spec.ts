import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrillaAlarmasComponent } from './grilla-alarmas.component';

describe('GrillaAlarmasComponent', () => {
  let component: GrillaAlarmasComponent;
  let fixture: ComponentFixture<GrillaAlarmasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrillaAlarmasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrillaAlarmasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
