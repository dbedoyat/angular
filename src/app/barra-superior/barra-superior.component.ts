import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../servicios/header.servicio';


@Component({
  selector: 'app-barra-superior',
  templateUrl: './barra-superior.component.html',
  styleUrls: ['./barra-superior.component.css']
})
export class BarraSuperiorComponent implements OnInit {

  name = 'Angular 6';
  title = '';

  constructor(private headerService: HeaderService) {}

  ngOnInit() {
    this.headerService.title.subscribe(title => {
      this.title = title;
    });
  }
}