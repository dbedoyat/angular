import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import { HeaderService } from '../servicios/header.servicio';
import {SelectionModel} from '@angular/cdk/collections';

export interface Food {
  value: string;
  viewValue: string;
}



export interface creaResponsable {
  sede_responsable: string;
  usuario_responsable: string;
  cargo_responsable:  string;  
  telefono_responsable:  number; 
  correo_responsable:  string; 
  web_responsable:  string; 
  perfil_responsable:  string; 


}

const ELEMENT_DATA: creaResponsable[] = [

{ sede_responsable: 'Bogotá - Sur',
  usuario_responsable: 'tejon',
  cargo_responsable: 'Gerente',
  telefono_responsable: 1111111,
  correo_responsable: 'correo@correo.com',
  web_responsable: 'permitido',
  perfil_responsable: 'Administrador',


},

{ sede_responsable: 'Bogotá - Sur',
usuario_responsable: 'tejon',
cargo_responsable: 'Gerente',
telefono_responsable: 1111111,
correo_responsable: 'correo@correo.com',
web_responsable: 'permitido',
perfil_responsable: 'Administrador',

},

{ sede_responsable: 'Bogotá - Sur',
usuario_responsable: 'tejon',
cargo_responsable: 'Gerente',
telefono_responsable: 1111111,
correo_responsable: 'correo@correo.com',
web_responsable: 'permitido',
perfil_responsable: 'Administrador',


},
];

@Component({
  selector: 'app-crear-responsable',
  templateUrl: './crear-responsable.component.html',
  styleUrls: ['./crear-responsable.component.css']
})


export class CrearResponsableComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

	
btnSave(event){
    event.target.classList.add('active');
    setTimeout(() => {

      event.target.classList.remove('active');
  }, 2900);  

}

isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataSource.data.length;
  return numSelected === numRows;
}

masterToggle() {
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
}	

displayedColumns: string[] = [ 'sede_responsable', 'usuario_responsable', 'cargo_responsable', 'telefono_responsable', 'correo_responsable', 'web_responsable', 'perfil_responsable', 'acciones_responsable',];
dataSource = new MatTableDataSource<creaResponsable>(ELEMENT_DATA);
selection = new SelectionModel<creaResponsable>(true, []);
applyFilter(filterValue: string) {
  this.dataSource.filter = filterValue.trim().toLowerCase();
}
	foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];	


  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.setTitle('Crear responsable');  
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }
}
