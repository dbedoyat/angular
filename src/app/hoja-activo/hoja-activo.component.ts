import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../servicios/header.servicio';
import { MatDialog } from '@angular/material';
import { SwiperComponent, SwiperDirective, SwiperConfigInterface,
  SwiperScrollbarInterface, SwiperPaginationInterface } from 'ngx-swiper-wrapper';


export interface Food {
  value: string;
  viewValue: string;
}

export interface atrVariables {
  atributo: string;
  valor: string;

}

const ELEMENT_DATA: atrVariables[] = [
  {atributo: 'Marca', valor: 'Marca1'},
  {atributo: 'Modelo', valor: 'Modelo1'},

];


@Component({
  selector: 'app-hoja-activo',
  templateUrl: './hoja-activo.component.html',
  styleUrls: ['./hoja-activo.component.css']
})
export class HojaActivoComponent implements OnInit {

  public config: SwiperConfigInterface = {
    a11y: true, 
    direction: 'horizontal',
    slidesPerView: 1,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    keyboard: true,
    mousewheel: true,
    scrollbar: false,
    pagination: true
  };



  displayedColumns: string[] = ['atributo', 'valor'];
  dataSource = ELEMENT_DATA;

  
  title: string = 'hoja-activo-map';
  lat: number = 4.1895251;
  lng: number = -74.4178952;


  
  constructor(private headerService: HeaderService, public dialog: MatDialog) {}
  
  
  openBajaActivo() {
    const dialogRef = this.dialog.open(BajaActivoHv, {panelClass: 'BajaActivo'});
 
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  ngOnInit() {

    this.headerService.setTitle('Hoja de vida activo');
  }
}

@Component({
  selector: 'BajaActivoHv',
  templateUrl: 'baja-activo.dialog.html',
})
export class BajaActivoHv {


  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

}
