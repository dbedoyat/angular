import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HojaActivoComponent } from './hoja-activo.component';

describe('HojaActivoComponent', () => {
  let component: HojaActivoComponent;
  let fixture: ComponentFixture<HojaActivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HojaActivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HojaActivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
