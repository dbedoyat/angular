import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../servicios/header.servicio';


export interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-informes',
  templateUrl: './informes.component.html',
  styleUrls: ['./informes.component.css']
})
export class InformesComponent implements OnInit {


  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  metricaList: string[] = ['Valor neto', 'No. Siniestros', 'Valor Siniestros', 'Repotenciaciones', 'Depreciación Local', 'Depreciación NIIF'];

  constructor(private headerService: HeaderService) { 

  }

  ngOnInit() {

    this.headerService.setTitle('Informes');
  }
}
