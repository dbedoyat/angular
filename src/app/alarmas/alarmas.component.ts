import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../servicios/header.servicio';

export interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-alarmas',
  templateUrl: './alarmas.component.html',
  styleUrls: ['./alarmas.component.css']
})
export class AlarmasComponent implements OnInit {
  btnSave(event){
    event.target.classList.add('active');
    setTimeout(() => {

      event.target.classList.remove('active');
  }, 2900);  

}
  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];
  constructor(private headerService: HeaderService) { }



  ngOnInit() {
    this.headerService.setTitle('Alarmas');
  }

}
