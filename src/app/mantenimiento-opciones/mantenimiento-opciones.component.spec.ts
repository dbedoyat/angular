import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MantenimientoOpcionesComponent } from './mantenimiento-opciones.component';

describe('MantenimientoOpcionesComponent', () => {
  let component: MantenimientoOpcionesComponent;
  let fixture: ComponentFixture<MantenimientoOpcionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MantenimientoOpcionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MantenimientoOpcionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
