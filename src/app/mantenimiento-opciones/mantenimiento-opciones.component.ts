import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../servicios/header.servicio';
import { MatDialog } from '@angular/material';

export interface Food {
  value: string;
  viewValue: string;
}



@Component({
  selector: 'app-mantenimiento-opciones',
  templateUrl: './mantenimiento-opciones.component.html',
  styleUrls: ['./mantenimiento-opciones.component.css']
})
export class MantenimientoOpcionesComponent implements OnInit {


  btnSave(event){
    event.target.classList.add('active');
    setTimeout(() => {

      event.target.classList.remove('active');
  }, 2900);  

}

	foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];	



  constructor(private headerService: HeaderService, public dialog: MatDialog) {}

  openBajaMantenimiento() {
   const dialogRef = this.dialog.open(BajaMantenimiento, {panelClass: 'BajaMantenimiento'});

   dialogRef.afterClosed().subscribe(result => {
     console.log(`Dialog result: ${result}`);
   });
 }

  ngOnInit() {
    this.headerService.setTitle('Opciones de mantenimiento');  
  }

}
@Component({
  selector: 'BajaMantenimiento',
  templateUrl: 'baja-activo.dialog.html',
})
export class BajaMantenimiento {


  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

}
